package me.vdev_.sacredcompass.prompts;

import me.vdev_.sacredcompass.SacredCompass;
import me.vdev_.sacredcompass.listeners.EventManager;
import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

/**
 * Second and final Prompt class.
 * Here the Location name is requested by the server. (Colors are optionally possible).
 * After that the Location Coords (that have been chosen with the admin tool) and the
 * Name given here are being stored in their respective slots in the config.yml.
 */

public class GetLocationName implements Prompt {

    String getLocationNameString;

    //slot to be chosen for the config. Here the value from the Map on GetSlot is used.
    String slot = "Location" + GetSlot.findLocationIndex.get("key");

    //Just avoiding duplicate code by using the variables created before.
    private void setThoseConfigValues() {
        SacredCompass.getInstance().getConfig().set(slot + ".Name", getLocationNameString);
        SacredCompass.getInstance().getConfig().set(slot + ".Coords", EventManager.location);
        SacredCompass.getInstance().saveConfig();
    }

    @Override
    public String getPromptText(ConversationContext context) {
        context.getSessionData("key");
        return (ChatColor.GREEN + "Type in the name of the Location:");
    }

    //Does not allow other messages to be passed on to the user.
    @Override
    public boolean blocksForInput(ConversationContext context) {
        return true;
    }

    /* Taking any Name given and assigning all the values to the config.
     * Also sending a message of summing-up to the admin.
     */
    @Override
    public Prompt acceptInput(ConversationContext context, String input) {
        context.setSessionData("key", input);
            getLocationNameString = (String)context.getSessionData("key");
                setThoseConfigValues();

        Player player = (Player) context.getForWhom();
            player.sendRawMessage(ChatColor.GREEN + "Location with coords:\n" +
                    ChatColor.RED + "x " + ChatColor.WHITE + "= " + ChatColor.YELLOW + EventManager.location.getX()
                    + ChatColor.RED + " y " + ChatColor.WHITE + "= " + ChatColor.YELLOW + EventManager.location.getY()
                    + ChatColor.RED + " z " + ChatColor.WHITE + "= " + ChatColor.YELLOW + EventManager.location.getZ()
                    + ChatColor.RED + " pitch " + ChatColor.WHITE + "= " + ChatColor.YELLOW + EventManager.location.getPitch()
                    + ChatColor.RED + " yaw " + ChatColor.WHITE + "= " + ChatColor.YELLOW + EventManager.location.getYaw()
                                        + ChatColor.GREEN + " has been set to slot " + ChatColor.AQUA + slot
                                                + ChatColor.GREEN + " with the name: " + ChatColor.AQUA + getLocationNameString
                                                        + ChatColor.GREEN + '.');
        return Prompt.END_OF_CONVERSATION;
    }
}
