package me.vdev_.sacredcompass.listeners;

import me.vdev_.sacredcompass.SacredCompass;
import me.vdev_.sacredcompass.commands.Sc;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class EventManager implements Listener {

    public static Location location;

    private int count = 1;
    private int leftCount = 1;

    /**
     * Below are the methods that are being used in the events to avoid duplicate code.
     */

    //Sets the Location when right clicking with compass.
    private void settingLoc (int count, Player player) {
        try {
            player.setCompassTarget(SacredCompass.getInstance().getConfig().getLocation("Location" + count + ".Coords"));
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText
                    (ChatColor.translateAlternateColorCodes('&', SacredCompass.getInstance().getConfig().getString("Location" + count + ".Name"))));
        } catch (NullPointerException ignored) {}
        this.count++;
    }

    //Sets the message provided to the user when left clicking with compass.
    private void messageOnLeftClick(int leftCount, Player player) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR , TextComponent.fromLegacyText
                (ChatColor.translateAlternateColorCodes('&', SacredCompass.getInstance().getConfig().getString("Location" + leftCount + ".Name"))));
    }

    //Checks for the conditions to be met in order for right click to work
    private boolean checkForOnClickWithCompass (ItemStack item, PlayerInteractEvent event) {
        return item.isSimilar(Sc.getSacredCompass())
                && item.getItemMeta().getCustomModelData() == 1357924680
                && ((event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getHand() == EquipmentSlot.HAND)
                || (event.getAction() == Action.RIGHT_CLICK_AIR && event.getHand() == EquipmentSlot.HAND));
    }

    private boolean checkForOnClickWithCompassOffHand (ItemStack item, PlayerInteractEvent event) {
        return item.isSimilar(Sc.getSacredCompass())
                && item.getItemMeta().getCustomModelData() == 1357924680
                && ((event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getHand() == EquipmentSlot.OFF_HAND)
                || (event.getAction() == Action.RIGHT_CLICK_AIR && event.getHand() == EquipmentSlot.OFF_HAND));
    }

    //Checks for the conditions to be met in order for left click to work
    private boolean checkForOnLeftClickWIthCompass (ItemStack item, PlayerInteractEvent event) {
        return item.isSimilar(Sc.getSacredCompass())
                && item.getItemMeta().getCustomModelData() == 1357924680
                && event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_AIR
                && (event.getHand() == EquipmentSlot.HAND || event.getHand() == EquipmentSlot.OFF_HAND);
    }

    //What happens when right clicking while holding the compass
    @EventHandler(priority = EventPriority.HIGH)
    public boolean onClickWithCompass (PlayerInteractEvent event) {

        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemStack offItem = player.getInventory().getItemInOffHand();

        while (((checkForOnClickWithCompass(item, event) || checkForOnClickWithCompassOffHand(offItem, event))
                && SacredCompass.getInstance().getConfig().getLocation("Location" + this.count + ".Coords") == null)) {
            count++;
            if (count == 5) {
                count = 1;
                break;
            }
        }

        if ((checkForOnClickWithCompass(item, event) || checkForOnClickWithCompassOffHand(offItem, event)) && count == 1) {
            settingLoc (count, player);
            leftCount = 1;
            return true;
        }
        else if ((checkForOnClickWithCompass(item, event) || checkForOnClickWithCompassOffHand(offItem, event)) && count == 2) {
            settingLoc (count, player);
            leftCount = 2;
            return true;
        }
        else if ((checkForOnClickWithCompass(item, event) || checkForOnClickWithCompassOffHand(offItem, event)) && count == 3) {
            settingLoc (count, player);
            leftCount = 3;
            return true;
        }
        else if ((checkForOnClickWithCompass(item, event) || checkForOnClickWithCompassOffHand(offItem, event)) && count == 4) {
            settingLoc (count, player);
            leftCount = 4;
            count = 1;
            return true;
        }
        return false;
    }

    //What happens when left clicking while holding compass
    @EventHandler (priority = EventPriority.HIGH)
    public boolean onLeftClickWithCompass (PlayerInteractEvent event) {

        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemStack offItem = player.getInventory().getItemInOffHand();

        if ((checkForOnLeftClickWIthCompass(item, event) || checkForOnLeftClickWIthCompass(offItem, event)) && leftCount == 1) {
            messageOnLeftClick(leftCount, player);
            return true;
        }
        else if ((checkForOnLeftClickWIthCompass(item, event) || checkForOnLeftClickWIthCompass(offItem, event)) && leftCount == 2) {
            messageOnLeftClick(leftCount, player);
            return true;
        }
        else if ((checkForOnLeftClickWIthCompass(item, event) || checkForOnLeftClickWIthCompass(offItem, event)) && leftCount == 3) {
            messageOnLeftClick(leftCount, player);
            return true;
        }
        else if ((checkForOnLeftClickWIthCompass(item, event) || checkForOnLeftClickWIthCompass(offItem, event)) && leftCount == 4) {
            messageOnLeftClick(leftCount, player);
            return true;
        }
        return false;
    }

    //What happens when switching to any other ItemStack, or air, or back to Sacred Compass without doing any clicking.
    @EventHandler
    public boolean onChangeToNormalCompass(PlayerItemHeldEvent event) {

        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItem(event.getNewSlot());
        ItemStack offItem = player.getInventory().getItem(event.getNewSlot());

        if (item == null || offItem == null){
            player.setCompassTarget(player.getWorld().getSpawnLocation());
            return true;
        }
        else if ((item.isSimilar(Sc.getSacredCompass()))
                || (offItem.isSimilar(Sc.getSacredCompass()))) {
            try {
                player.setCompassTarget(SacredCompass.getInstance().getConfig().getLocation("Location" + leftCount + ".Coords"));
            } catch (NullPointerException ignored) {}
            return true;
        }
        else {
            player.setCompassTarget(player.getWorld().getSpawnLocation());
            return true;
        }
    }

    //What happens when right clicking while holding the admin tool
    @EventHandler
    public boolean onClickWithWand(PlayerInteractEvent event) {

        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();

        if (item.isSimilar(Sc.getLocationSelectionTool()) && player.getInventory().getItemInMainHand().getItemMeta().getCustomModelData() == 923546780
                && event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getHand() == EquipmentSlot.HAND) {
            location = event.getClickedBlock().getLocation();
            player.sendMessage(ChatColor.DARK_PURPLE + "Location has been set to: \n"
                    + ChatColor.RED + "x " + ChatColor.WHITE + "= " + ChatColor.YELLOW + event.getClickedBlock().getLocation().getX()
                    + ChatColor.RED + " y " + ChatColor.WHITE + "= " + ChatColor.YELLOW + event.getClickedBlock().getLocation().getY()
                    + ChatColor.RED + " z " + ChatColor.WHITE + "= " + ChatColor.YELLOW + event.getClickedBlock().getLocation().getZ()
                    + ChatColor.RED + " pitch " + ChatColor.WHITE + "= " + ChatColor.YELLOW + event.getClickedBlock().getLocation().getPitch()
                    + ChatColor.RED + " yaw " + ChatColor.WHITE + "= " + ChatColor.YELLOW + event.getClickedBlock().getLocation().getYaw());
            return true;
        }
        return false;
    }
}