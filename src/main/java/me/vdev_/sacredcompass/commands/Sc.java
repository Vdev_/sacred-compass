package me.vdev_.sacredcompass.commands;

import me.vdev_.sacredcompass.prompts.DelSlot;
import me.vdev_.sacredcompass.prompts.GetSlot;
import me.vdev_.sacredcompass.SacredCompass;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Basic command "/sc"
 * Possible arguments:
 *              "tool": gives the admin tool
 *              "compass": gives the Sacred Compass to the user or admin
 *              "setloc": opens a conversation with the admin to assist saving the location selected with the admin tool.
 *              "help": gives general help about the commands.
 *              "delloc": deletes the info of a certain location allowing the compass to skip it.
 *              NOTE: "setloc" needs the tool too be used first!
 */

public class Sc implements CommandExecutor {

    public static ItemStack locationSelectionTool = new ItemStack(Material.STICK, 1);
    public static ItemStack sacredCompass = new ItemStack(Material.COMPASS, 1);

    public static ItemStack getLocationSelectionTool() {
        ItemMeta meta = locationSelectionTool.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Location Selection Tool");
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.setCustomModelData(923546780);
        locationSelectionTool.setItemMeta(meta);
        return locationSelectionTool;
    }

    public static ItemStack getSacredCompass() {
        ItemMeta meta = sacredCompass.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_BLUE + "Sacred " + ChatColor.DARK_RED + "Compass");
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.setCustomModelData(1357924680);
        sacredCompass.setItemMeta(meta);
        return sacredCompass;
    }

    private final ConversationFactory tryingToGetThePosition = new ConversationFactory(SacredCompass.getInstance())
            .withFirstPrompt(new GetSlot())
            .withEscapeSequence("exit")
            .withModality(true)
            .withTimeout(30);

    private final ConversationFactory toDeleteTheSlot = new ConversationFactory(SacredCompass.getInstance())
            .withFirstPrompt(new DelSlot())
            .withEscapeSequence("exit")
            .withModality(true)
            .withTimeout(30);

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("Please use this plugin's commands in-game");
        } else {
            Player player = (Player) sender;

            if (args.length == 0) {
                player.sendMessage(ChatColor.RED + "Very few arguments\n"
                        + ChatColor.YELLOW + "For list of arguments type " + ChatColor.LIGHT_PURPLE + "\"/sc help\"");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("tool") && player.hasPermission("sacredcompass.admin")) {
                    player.sendMessage(ChatColor.GREEN + "You have gained a " + ChatColor.AQUA + "Location Selection Tool");
                    player.getInventory().addItem(getLocationSelectionTool());
                } else if (args[0].equalsIgnoreCase("compass") && player.hasPermission("sacredcompass.player")) {
                    player.sendMessage(ChatColor.GREEN + "You have been granted the " + ChatColor.DARK_BLUE + "Sacred " + ChatColor.DARK_RED + "Compass"
                                            + ChatColor.GREEN + '!');
                    player.getInventory().addItem(getSacredCompass());
                } else if (args[0].equalsIgnoreCase("setloc") && player.hasPermission("sacredcompass.admin") && (!player.isConversing())) {
                    tryingToGetThePosition.buildConversation(player).begin();
                } else if (args[0].equalsIgnoreCase("delloc") && player.hasPermission("sacredcompass.admin") && (!player.isConversing())) {
                    toDeleteTheSlot.buildConversation(player).begin();
                } else if (args[0].equalsIgnoreCase("help")) {
                    if (player.hasPermission("sacredcompass.tool") && player.hasPermission("sacredCompass.admin")) {
                        player.sendMessage(ChatColor.YELLOW + "/sc tool" + ChatColor.GRAY + ": " + ChatColor.WHITE
                                                + "with this admin tool you can right click on any location to be able to use /setloc afterwards.\n"
                                         + ChatColor.YELLOW + "/sc setloc" + ChatColor.GRAY + ": " + ChatColor.WHITE
                                                + "with this command store the location coords and Name of choice in the config.\n"
                                         + ChatColor.YELLOW + "/sc compass" + ChatColor.GRAY + ": " + ChatColor.WHITE
                                                + "this is the normal command to get the " + ChatColor.DARK_BLUE + "Sacred " + ChatColor.DARK_RED + "Compass!\n"
                                         + ChatColor.YELLOW + "/sc delloc" + ChatColor.GRAY + ": " + ChatColor.WHITE
                                                + "use this to delete a locations data in the config. That means that the compass will skip the location deleted"
                                                        + " and will act like it does exist ;)");
                    } else {
                        player.sendMessage(ChatColor.YELLOW + "/sc compass" + ChatColor.GRAY + ": " + ChatColor.WHITE
                                                + "Use this command and obtain the " + ChatColor.DARK_BLUE + "Sacred " + ChatColor.DARK_RED + "Compass!");
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + "Too many arguments\n"
                        + ChatColor.YELLOW + "For list of arguments type " + ChatColor.LIGHT_PURPLE + "\"/sc help\"");
            } return true;
        }
        return false;
    }
}
