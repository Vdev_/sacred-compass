/**
 *     Sacred Compass
 *     Author Vdev_
 *     Version 1.0
 *     Adds a custom compass that allows pointing of 4 Locations when clicking
 *     Optional Dependencies: Any chat plugin that enables colored chat with '&' prefix
 *     if you want to add colored names to the Locations.
 */

package me.vdev_.sacredcompass;

import me.vdev_.sacredcompass.commands.Sc;
import me.vdev_.sacredcompass.listeners.EventManager;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class SacredCompass extends JavaPlugin{
    private static SacredCompass instance;
    public static SacredCompass getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {

        instance = this;
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        getCommand("sc").setExecutor(new Sc());
        this.getServer().getPluginManager().registerEvents(new EventManager(), this);
        System.out.println(ChatColor.AQUA + "SacredCompass" + ChatColor.GREEN + " up and running!");

    }
}

