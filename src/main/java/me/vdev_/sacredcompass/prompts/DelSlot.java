package me.vdev_.sacredcompass.prompts;

import me.vdev_.sacredcompass.SacredCompass;
import me.vdev_.sacredcompass.commands.Sc;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.NumericPrompt;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

/**
 * Prompt about deleting a Location. Here we accept integers from 1 to 4 again,
 * but this time to erase data from a Location, allowing the compass not to point towards that location,
 * while keeping the slot empty in the config.yml.
 */

public class DelSlot extends NumericPrompt implements Cloneable {

    @Override
    public String getPromptText(ConversationContext context) {

        return ChatColor.GREEN + "Please enter the " + ChatColor.RED + "slot "
                    + ChatColor.GREEN + "you want to erase.";
    }

    @Override
    protected boolean isInputValid(ConversationContext context, String input) {

        Player player = (Player) context.getForWhom();

        if (NumberUtils.isNumber(input) && isNumberValid(context, NumberUtils.createNumber(input))) {
            return true;
        }
        else
            player.sendRawMessage(ChatColor.RED + "Please enter an integer.");
        return false;
    }

    @Override
    protected boolean isNumberValid(ConversationContext context, Number input) {
        return input.intValue() > 0 && input.intValue() <= 4;
    }

    //Self-explanatory
    @Override
    protected String getFailedValidationText(ConversationContext context, Number invalidInput) {
        return ChatColor.RED + "Input must be between 1 and 4.";
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, Number input) {

        Player player = (Player) context.getForWhom();
        context.setSessionData("key", input.intValue());

        if (SacredCompass.getInstance().getConfig().get("Location" + input + ".Name") != null) {
            SacredCompass.getInstance().getConfig().getConfigurationSection("Location" + input).getKeys(false)
                    .forEach(key -> SacredCompass.getInstance().getConfig().getConfigurationSection("Location" + input).set(key, null));
            SacredCompass.getInstance().saveConfig();
            player.sendRawMessage(ChatColor.LIGHT_PURPLE + "Location" + input + " values have been deleted."
                    + " Now the " + Sc.getSacredCompass().getItemMeta().getDisplayName()
                    + ChatColor.LIGHT_PURPLE + " will ignore that Location when right clicked!");
        } else player.sendRawMessage(ChatColor.RED + "No data was found in that slot.");
        return Prompt.END_OF_CONVERSATION;
    }
}
