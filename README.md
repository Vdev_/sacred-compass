This plugin provides the addition of a new custom compass that can point to up to 4 different locations (while the normal compass keeps pointing towards the spawn). Should be a good fit for survival or rpg-survival type servers that don't want to have teleportations to add a bit extra challenge and longevity.

How to use it:

Permissions:
- sacredcompass.admin  (/sc tool, /sc setloc)
- sacredcompass.player  (/sc compass)

Usage:

As admin first you need to do /sc tool to obtain the location selection tool. To do that right click a block (any placed block in the world) and a message with the coords will appear. That means that the coordinates of the clicked block have been stored temporarily for use. Then use /sc setloc and you will be prompted to enter an integer in chat. That integer will be used as the number of the Location slot in the config.yml (you have options from 1 to 4 for slot and can change the slot values by going through this process anytime or through the config). Then you will be prompted to enter a name for the specific Location (colors accepted but depend on a chat plugin that adds colored chat). You are done.
NOTE: For now to delete a Location you have to do it manually through the config. Now let's go see how the compass actually works.

As a player you can get the Sacred Compass by doing /sc compass.
Each time a player holds the compass the compass points to the latest location pointed. If the server restarts the compass points to Location slot 1 by default. When switching to anything else than the compass (even normal compass) every compass including the sacred compass points towards the world spawn Location.
Each time you right click while holding the compass in any of your hands (MainHand or OffHand) the needle changes the direction it points to pointing to the next slot set in the config.yml. If for example you have set slot 1 as "Mordor" and slot 4 as "DarkDungeon" it points to 1, then jumps through 2 and 3, then points to 4 immediately and if you right click goes back to 1. That's it. And that's why I have only four possible locations and not more (would be a pain if someone had to click 9 times to look for the next direction they wanted in my opinion). Also along with the needle changing direction, the name of the new location pointed is shown to the user on their Action Bar (name being the name set by using the admin tool.
Left click while holding the compass to view the Name of the location it's pointing to on the Action Bar.

If any bugs are found or you might want an addition send me a message on
- Discord: Vdev_#5031
