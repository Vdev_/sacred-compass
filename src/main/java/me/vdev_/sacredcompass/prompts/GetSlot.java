package me.vdev_.sacredcompass.prompts;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * First Prompt class, takes an integer input in order to set the slot of the Location in the config.yml
 * Checks if the input is integer, then if it is between 1 and 4, and if it's not
 * then prompts the user again.
 * Sends the user to GetLocationName Prompt when all steps are completed.
 */

public class GetSlot extends NumericPrompt implements Cloneable {

    //map that stores the integer value to be used on the next Prompt.
    public static Map <String, Integer> findLocationIndex = new HashMap<>();

    public String getPromptText(ConversationContext context) {
            context.getSessionData("key");
                    return ChatColor.GREEN + "Type in the slot of the location." + '\n'
                            + ChatColor.YELLOW + "Available slots " + '\n' + ChatColor.AQUA + "1 "
                                    + ChatColor.YELLOW + "to " + ChatColor.AQUA + "4 " + ChatColor.YELLOW + ':';
    }

    //Does not allow other messages to be passed on to the user.
    @Override
    public boolean blocksForInput(ConversationContext context) {
        return true;
    }


    //Self-explanatory
    @Override
    protected boolean isInputValid(ConversationContext context, String input) {
        Player player = (Player) context.getForWhom();

        if (NumberUtils.isNumber(input) && isNumberValid(context, NumberUtils.createNumber(input))) {
            return true;
        }
        else if (!NumberUtils.isNumber(input))
            player.sendRawMessage(ChatColor.RED + "Please enter an integer.");
        return false;
    }

    //Self-explanatory
    @Override
    protected boolean isNumberValid(ConversationContext context, Number input) {
        return input.intValue() > 0 && input.intValue() <= 4;
    }

    //Self-explanatory
    @Override
    protected String getFailedValidationText(ConversationContext context, Number invalidInput) {
        return "Input must be between 1 and 4.";
    }

    //Stores the input to the Map on top
    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, Number number) {
        context.setSessionData("key", number.intValue());
            findLocationIndex.put("key", (Integer)context.getSessionData("key"));
        return new GetLocationName();
    }
}